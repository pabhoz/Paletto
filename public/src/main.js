fetch('../db/palettes.json')
    .then(response => response.json())
    .then(palettes => {

        const selectedColor = {
            value: undefined,
            model: 'hex'
        };

        palettes.forEach(palette => {
            createPalette(palette);
        });

        function createPalette(palette) {
            const paletteEl = document.createElement("div");
            paletteEl.classList.add('palette');

            const colors = document.createElement("div");
            colors.classList.add('colors');

            palette.colors.forEach(color => {
                const colorEl = document.createElement("div");
                colorEl.classList.add('color');
                colorEl.style.backgroundColor = color.codeHex;
                colorEl.dataset.hex = color.codeHex;
                colorEl.onclick = function () {
                    const paletteEl = this.parentElement.parentElement;
                    const input = paletteEl.querySelector('input');
                    selectedColor.value = this.dataset.hex;
                    input.value = this.dataset.hex;
                }

                colors.appendChild(colorEl);
            })

            const codes = document.createElement("div");
            codes.classList.add('codes');

            const colorModels = document.createElement("div");
            colorModels.classList.add('colorModels');

            const hex = document.createElement("div");
            hex.classList.add('selected');
            hex.innerHTML = 'HEX';
            const rgb = document.createElement("div");
            rgb.innerHTML = 'RGB';
            rgb.onclick = function () {
                const input = this.parentElement.parentElement.parentElement.querySelector("input");
                const value = fromHexToRGB(selectedColor.value);
                input.value = `${value.r},${value.g},${value.b}`;
                selectColorModel(this);
            }
            const hsl = document.createElement("div");
            hsl.innerHTML = 'HSL';

            colorModels.appendChild(hex);
            colorModels.appendChild(rgb);
            colorModels.appendChild(hsl);

            codes.appendChild(colorModels);

            const value = document.createElement("div");
            value.classList.add('value');
            const input = document.createElement("input");
            input.type = "text";

            value.appendChild(input);
            codes.appendChild(value);

            paletteEl.appendChild(colors);
            paletteEl.appendChild(codes);
            document.body.appendChild(paletteEl);
        }
        function fromHexToRGB(hex) {
            var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
            return result ? {
                r: parseInt(result[1], 16),
                g: parseInt(result[2], 16),
                b: parseInt(result[3], 16)
            } : null;
        }

        function selectColorModel(el) {
            const colorModels = el.parentElement;
            const models = colorModels.querySelectorAll('div');
            models.forEach(model => {
                model.classList.remove('selected');
            });
            el.classList.add('selected');
        }
    });
